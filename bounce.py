import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.spatial.distance import pdist, squareform

N = 50

# 100 particles, with x,y,vx,vy
dots = np.random.rand(N, 4)
# velocities on -0.5,0.5
dots[:, 2:] -= 0.5
# scale velocities
dots[:, 2:] *= 0.01


def main():
    fig, ax = plt.subplots()

    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)

    xs = dots[:, 0]
    ys = dots[:, 1]

    line, = ax.plot([], [], 'b.', ms=16)

    def animate(i):
        timestep()
        line.set_data(xs, ys)  # update the data
        return line,

    ani = animation.FuncAnimation(fig, animate,
                                  interval=25,
                                  blit=True)

    # ani.save('basic_animation.mp4', fps=30,
    #          extra_args=['-vcodec', 'libx264'])
    # or
    plt.show()


def timestep():

    distance = squareform(pdist(dots[:, :2]))

    collision = (distance < 0.03) & (distance != 0)

    (indices1, indices2) = np.where(collision)

    for i in range(len(indices1)//2):
        aux = dots[indices1[i], 2:].copy()
        dots[indices1[i], 2:] = dots[indices2[i], 2:].copy()
        dots[indices2[i], 2:] = aux

    dots[:, 0] += dots[:, 2]
    dots[:, 1] += dots[:, 3] - 0.5*g
    dots[:, 3] -= g

    touching_floor_or_top = (dots[:, 1] < 0.0) | (dots[:, 1] > 1.0)
    touching_right_or_left = (dots[:, 0] < 0.0) | (dots[:, 0] > 1.0)

    dots[touching_floor_or_top, 3] *= -1.0

    dots[touching_right_or_left, 2] *= -1.0


g = 0.0001

main()
