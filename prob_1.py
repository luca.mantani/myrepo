def is_multiple(n):
    "Tells you if the number is multiple of 3 or 5"
    return n % 3 == 0 or n % 5 == 0


lista = []

for i in range(1, 1000):
    if is_multiple(i):
        lista.append(i)

somma = sum(lista)

print(somma)
