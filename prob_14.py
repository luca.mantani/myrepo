def rule(number):
    if number == 1:
        return number
    elif number % 2 == 0:
        return number // 2
    else:
        return 3*number+1


def lenght_sequence(n):
    if n in dictionary:
        return dictionary[n]
    else:
        return 1 + lenght_sequence(rule(n))


longest = 0
index = 0
maxim = 1000000

dictionary = {1: 1}

for i in range(1, maxim):
    lung = lenght_sequence(i)
    if lung > longest:
        longest = lung
        index = i
    dictionary[i] = lung

print("The longest is ", index, " , the sequence is ", longest, " long\n")
